##### Project 7 #####
### Bayes' Network ###
### Michael Hesseltine, Tim Lowther, Erik Weimer ###
import time
import random

user_age = 0

user_weight = 0
user_height = ""
user_wt_class = ""

user_elderly = False
user_gen_disp = False
user_hasAsthma = False
user_hasCancer = False
user_hasHyperTension = False
user_hasBacteria = False

hyptenProb = 0
canProb = 0
asthmaProb = 0
fluProb = 0
coldProb = 0
infProb = 0


heights = []

def initialize_BMI_lookup():
    ###Initialize heights
    counter = 0
    for feet in range(4,7):
        for inches in range(0,12):
            if (counter > 9 and counter < 31):
                heights.append(str(feet) + " ft " + str(inches) + " in")
            counter += 1

    ###Initialize a BMI lookup
    myDict = {}
     
    myDict[heights[0]] = list_of_pairs(91,118,119,142,143,190,191,1000000)
    myDict[heights[1]] = list_of_pairs(94,123,124,147,148,197,198,1000000)
    myDict[heights[2]] = list_of_pairs(97,127,128,152,153,203,204,1000000)
    myDict[heights[3]] = list_of_pairs(100,131,132,157,158,210,211,1000000)
    myDict[heights[4]] = list_of_pairs(104,135,136,163,164,217,218,1000000)
    myDict[heights[5]] = list_of_pairs(107,140,141,168,169,224,225,1000000)
    myDict[heights[6]] = list_of_pairs(110,144,145,173,174,231,232,1000000)
    myDict[heights[7]] = list_of_pairs(114,149,150,179,180,239,240,1000000)
    myDict[heights[8]] = list_of_pairs(118,154,155,185,186,246,247,1000000)
    myDict[heights[9]] = list_of_pairs(121,158,159,190,191,254,255,1000000)
    myDict[heights[10]] = list_of_pairs(125,163,164,196,197,261,262,1000000)
    myDict[heights[11]] = list_of_pairs(128,168,169,202,203,269,270,1000000)
    myDict[heights[12]] = list_of_pairs(132,173,174,208,209,277,278,1000000)
    myDict[heights[13]] = list_of_pairs(136,178,179,214,215,285,286,1000000)
    myDict[heights[14]] = list_of_pairs(140,183,184,220,221,293,294,1000000)
    myDict[heights[15]] = list_of_pairs(144,188,189,226,227,301,302,1000000)
    myDict[heights[16]] = list_of_pairs(148,193,194,232,233,310,311,1000000)
    myDict[heights[17]] = list_of_pairs(152,199,200,239,240,318,319,1000000)
    myDict[heights[18]] = list_of_pairs(156,204,205,245,246,327,328,1000000)

    return myDict    
            
def determine_if_true(condition):
    if(condition == "bacteria"):
        user_input = input("Are you regularly exposed to large amounts of bacteria? (Y/N): ")
    else:
        user_input = input("Does your family have a history of "+condition+"? (Y/N): ")
        
    if(user_input.lower() == "y"):
        return True
    else:
        return False
    
    
            
def list_of_pairs(n1,n2,ov1,ov2,ob1,ob2,e1,e2):
    pairs = []
    pairs.append([n1,n2])
    pairs.append([ov1,ov2])
    pairs.append([ob1,ob2])
    pairs.append([e1,e2])
    return pairs

def adjust_based_on_weight(weightClass,bacteria):
    global hyptenProb
    global canProb
    global asthmaProb
    global fluProb
    global coldProb
    global infProb
    
    if(bacteria == True):
        if(weightClass == "underweight" or weightClass == "normal weight"
           or weightClass == "overweight"):
            hyptenProb -= 10
            canProb += 2
            asthmaProb += 2
            fluProb += 2
            coldProb += 2
            infProb += 2
        else:
            hyptenProb += 10
            canProb -= 2
            asthmaProb -= 2
            fluProb -= 2
            coldProb -= 2
            infProb -= 2
    else:
        if(weightClass == "underweight" or weightClass == "normal weight"
           or weightClass == "overweight"):
            hyptenProb -= 10
            canProb += 5
            asthmaProb += 5
        else:
            hyptenProb += 10
            canProb -= 5
            asthmaProb -= 5
            

def diagnose(bacteria):
    global user_wt_class
    probability = random.randint(1,100)
    
    if(bacteria == True):
        if(user_wt_class == "underweight" or user_wt_class == "normal weight"
           or user_wt_class == "overweight"):
            if(probability < 7):
                return "hypertension"
            else:
                random_selection = random.randint(1,5)
                if(random_selection == 1):
                    return "cancer"
                elif(random_selection == 2):
                    return "asthma"
                elif(random_selection == 3):
                    return "the flu"
                elif(random_selection == 4):
                    return "a cold"
                else:
                    return "an infection"
        else:
            if(probability < 27):
                return "hypertension"
            else:
                random_selection = random.randint(1,5)
                if(random_selection == 1):
                    return "cancer"
                elif(random_selection == 2):
                    return "asthma"
                elif(random_selection == 3):
                    return "the flu"
                elif(random_selection == 4):
                    return "a cold"
                else:
                    return "an infection"
    else:
        if(user_wt_class == "underweight" or user_wt_class == "normal weight"
           or user_wt_class == "overweight"):
            if(probability < 24):
                return "hypertension"
            else:
                random_selection = random.randint(1,2)
                if(random_selection == 1):
                    return "cancer"
                else:
                    return "asthma"
        else:
            if(probability < 44):
                return "hypertension"
            else:
                random_selection = random.randint(1,2)
                if(random_selection == 1):
                    return "cancer"
                else:
                    return "asthma"

def display_causal_chain(diag):
    if(diag == "hypertension"):
        print("Because of your hypertension, you are at risk for cardiac arrest, which could lead to death and then rigormortis.")
    elif(diag == "cancer"):
        print("Because of your cancer, you are at risk for death, which could lead to rigormortis.")
    elif(diag == "asthma"):
        rng = random.randint(1,100)
        if(rng < 10):
            print("Because of your asthma, you are at risk for an asthma attack, which could lead to death and then rigormortis.")
        else:
            print("Because of your asthma, you may develop a fever.")
    elif(diag == "the flu"):
        rng = random.randint(1,100)
        flustring = "Because of the flu, you may experience "
        if(rng <= 20):
            flustring += "mucus buildup."
        elif(rng <= 40):
            flustring += "sneezing."
        elif(rng <= 60):
            flustring += "coughing."
        elif(rng <= 80):
            flustring += "a fever."
        else:
            flustring += "pneumonia. Your pneumonia may lead to "
            rng = random.randint(1,100)
            if(rng <= 20):
                flustring += "a fever."
            elif(rng <= 40):
                flustring += "coughing."
            elif(rng <= 60):
                flustring += "sneezing."
            elif(rng <= 80):
                flustring += "mucus buildup."
            else:
                flustring += "death, which will result in rigormortis."
        print(flustring)
    elif(diag == "a cold"):
        rng = random.randint(1,100)
        coldstring = "Because of your cold, you may experience "
        if(rng <= 33):
            coldstring += " a fever."
        elif(rng <= 66):
            coldstring += " sneezing."
        else:
            coldstring += " mucus buildup."
        print(coldstring)
    else:
        rng = random.randint(1,100)
        infstring = "Because of your infection, you may experience "
        if(rng <= 33):
            infstring += " death which would result in rigormortis."
        elif(rng <= 66):
            infstring += " pus accumulation."
        else:
            infstring += " a fever."
        print(infstring)
    
def main():
    global user_elderly
    global user_gen_disp 
    global user_hasAsthma 
    global user_hasCancer 
    global user_hasHyperTension 
    global user_hasBacteria
    global user_wt_class

    global hyptenProb
    global canProb
    global asthmaProb
    global fluProb
    global coldProb
    global infProb


    ###Initialize BMI Lookup
    BMI_dictionary = initialize_BMI_lookup()
    print("Hello! I am Online Doctor Bot! I would be happy to be of assistance!\n")

    ###Capture Age
    user_age = int(input("Please enter your age: "))
    if(user_age >= 62):
        ###Determine if elderly
        user_elderly = True

    ###Determine weight
    user_weight = int(input("Please enter your weight (lbs.): "))
    
    user_height_feet = (input("Please enter your height (ft.) : "))
    user_height_inches = (input("Please enter your height (in.) : "))
    ###Parse weight
    user_height = user_height_feet + " ft " + user_height_inches + " in"

    ###Determine if the user has a genetic dispostion
    user_gen_disp = determine_if_true("genetic disposition")
    
    if(user_gen_disp == True):
        user_hasAsthma = determine_if_true("asthma")
        user_hasCancer = determine_if_true("cancer")
        user_hasHypertension = determine_if_true("hypertension")
        user_hasBacteria = determine_if_true("bacteria")

    
    if(user_hasBacteria == True):
        hyptenProb = 16.66
        canProb = 16.66
        asthmaProb = 16.66
        fluProb = 16.66
        coldProb = 16.66
        infProb = 16.66
        adjust_based_on_weight(user_wt_class,user_hasBacteria)
    else:
        hyptenProb = 33.33
        canProb = 33.33
        asthmaProb = 33.33
        adjust_based_on_weight(user_wt_class,user_hasBacteria)

    print("\nEvaluating......")
    time.sleep(2)
    
    if(user_elderly == False):
        print("You are not considered elderly.")
    else:
        print("You are considered elderly")

    user_wt_class = determine_weight_class(user_height,user_weight,BMI_dictionary)
    print("It appears you are",user_wt_class)

    diagnosis = diagnose(user_hasBacteria)

    print("Based on your responses, I diagose you with",diagnosis)
    display_causal_chain(diagnosis)
    



def determine_weight_class(ht,wt,bmi_dict):
    if(wt >= bmi_dict[ht][0][0] and wt <= bmi_dict[ht][0][1]):
        return "normal weight"
    elif(wt >= bmi_dict[ht][1][0] and wt <= bmi_dict[ht][1][1]):
        return "overweight"
    elif(wt >= bmi_dict[ht][2][0] and wt <= bmi_dict[ht][2][1]):
        return "obese"
    elif(wt >= bmi_dict[ht][3][0] and wt <= bmi_dict[ht][3][1]):
        return "extremely obese"
    else:
        return "underweight"
    

main()

