import random

phase1 = True
phase2 = False
phase3 = False
bossHealth = 100
playerHealth = 500

def playerAttack():
        global bossHealth
        damage = random.randint(1,15)
        print("You attack back! " + str(damage) + " points of damage. ", end="")
        bossHealth -= damage
        print("Boss has " + str(bossHealth) + "% health remaining")

def bossAttack1():
        global playerHealth
        tailSwingDamage = random.randint(4,7)
        print("Boss is Performing Tail Swing! " + str(tailSwingDamage) + " points of damage. ", end="")
        playerHealth -= tailSwingDamage
        print("You have " + str(playerHealth) + " health remaining")
        #playerAttack(bossHealth)
        playerAttack()

def bossAttack2():
        global playerHealth
        stompDamage = random.randint(7, 10)
        print("Boss is Performing Stomp! " + str(stompDamage) + " points of damage. ", end="")
        playerHealth -= stompDamage
        print("You have " + str(playerHealth) + " health remaining")
        #playerAttack(bossHealth)
        playerAttack()

def bossAttack3():
        global playerHealth
        lazerEyesDamage = random.randint(10, 13)
        print("Boss is Performing Lazer Eyes! " + str(lazerEyesDamage) + " points of damage. ", end="")
        playerHealth -= lazerEyesDamage
        print("You have " + str(playerHealth) + " health remaining")
        #playerAttack(bossHealth)
        playerAttack()
         
def changePhase(previousPhase, currentPhase):
        global phase1
        global phase2
        global phase3

        if(previousPhase == 1):
                phase1 = False
        if(previousPhase == 2):
                phase2 = False
        if(previousPhase == 3):
                phase3 = False
        
        if(currentPhase == 1):
                phase1 = True
        if(currentPhase == 2):
                phase2 = True
        if(currentPhase == 3):
                phase3 = True
        
        print("Changing phase from phase " + str(previousPhase), end="")
        print(" to phase " + str(currentPhase))
        
def bossFight():
        while(bossHealth > 0):
                ## PHASE 1 ##
                if(phase1):
                        if(bossHealth > 75):
                                bossAttack1()
                        else:
                                phaseChange12 = random.randint(1,100)
                                if (phaseChange12 >= 11):
                                        changePhase(1, 2)
                                        bossAttack2()
                                elif(phaseChange12 <= 10):
                                        bossAttack1()
                                        
                ## PHASE 2 ##
                if(phase2):
                        if(bossHealth > 50):
                                randomAttack12 = random.randint(1,100)
                                if (randomAttack12 <= 50):
                                        bossAttack1()
                                elif(randomAttack12 > 50):
                                        bossAttack2()
                        if(bossHealth <= 50):
                                print("Boss is at 50% or lower! Evaluating state change...")
                                phaseChange23 = random.randint(1,100)
                                if(phaseChange23 <= 5):
                                        changePhase(2, 1)
                                if(phaseChange23 > 5 and phaseChange23 <= 10):
                                        print("Remaining in phase 2")
                                if(phaseChange23 > 10):
                                        changePhase(2, 3)
                ## PHASE 3 ##
                if(phase3):
                        randomAttack = random.randint(1,100)
                        randomPhaseChange = random.randint(1,100)
                        if(randomAttack <= 33):
                                bossAttack1()
                        if(randomAttack > 33 and randomAttack <= 66):
                                bossAttack2()
                        if(randomAttack >= 67):
                                bossAttack3()
                        print("Evaluating phase change...")
                        if(randomPhaseChange <= 10):
                                changePhase(3,2)
                        if(randomPhaseChange > 10):
                                print("Remaining in Phase 3!")
                        
        print("Boss has been defeated! You had " + str(playerHealth) + " hit points remaining.")
                                
bossFight()


















