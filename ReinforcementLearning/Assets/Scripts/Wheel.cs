﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel : MonoBehaviour {

    public GameObject pole;
    HingeJoint wheel;
    JointMotor motor;
    public bool movingRight;

    private void Start()
    {
        wheel = GetComponent<HingeJoint>();
        movingRight = false;
    }

    // Update is called once per frame
    void Update ()
    {
		if(!movingRight && 90 - pole.transform.eulerAngles.z < -110)
        {
            Debug.Log("Moving Right!");
            motor = wheel.motor;
            motor.targetVelocity *= -1;
            wheel.motor = motor;
            movingRight = true;
            
        }

        else if(movingRight && 90 - pole.transform.eulerAngles.z > -80)
        {
            Debug.Log("Moving Left!");
            motor = wheel.motor;
            motor.targetVelocity *= -1;
            wheel.motor = motor;
            movingRight = false;
        }

        Debug.Log(90 - pole.transform.eulerAngles.z);
        
    }
}
