﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Player
{
    public Image panel;
    public Text text;
}

[System.Serializable]
public class PlayerColor
{
    public Color panelColor;
    public Color textColor;
}

public class GameController : MonoBehaviour
{

    public Text[] buttonList;
    public GameObject gameOverPanel;
    public Text gameOverText;
    public GameObject restartButton;
    public Player playerX;
    public Player playerO;
    public PlayerColor activePlayerColor;
    public PlayerColor inactivePlayerColor;
    public bool playerMove;
    public float delay;

    private string playerSide = "X";
    private string computerSide = "O";

    private int moveCount;
    private int value;

    void Awake()
    {
        //Function that runs once the game is started up
        SetGameControllerReferenceOnButtons();
        gameOverPanel.SetActive(false);
        moveCount = 0;
        RandomizeStart();
    }

    private void Update()
    {
        //Updates every frame of gameplay
        if(playerMove == false && gameOverPanel.activeInHierarchy == false)
        {
            //Delay makes it so the AI does not pick immediately after the player.
            delay += delay * Time.deltaTime;
            if(delay >= 100)
            {
                //Purpose of AI selection is to assign a number to the value variable
                AI_Selection();
                if(buttonList[value].GetComponentInParent<Button>().interactable == true)
                {
                    buttonList[value].text = GetComputerSide();
                    buttonList[value].GetComponentInParent<Button>().interactable = false;
                    EndTurn();
                }
            }
        }
    }

    void SetGameControllerReferenceOnButtons()
    {
        for (int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].GetComponentInParent<GridSpace>().SetGameControllerReference(this);
        }
    }

    void StartGame()
    {
        SetBoardInteractable(true);
    }

    public void SetStartingSide(string startingSide)
    {
        //Only purpose is to set the player colors based on RNG
        if(startingSide == "X")
        {
            SetPlayerColors(playerX, playerO);
        }
        else
        {
            SetPlayerColors(playerO, playerX);
        }
        StartGame();
    }

    public string GetPlayerSide()
    {
        return playerSide;
    }

    public string GetComputerSide()
    {
        return computerSide;
    }

    public void EndTurn()
    {
        moveCount++;
        bool playerWin = WinDetected(playerSide);
        bool compWin = WinDetected(computerSide);
        if(playerWin)
        {
            GameOver(playerSide);
        }
        else if(compWin)
        {
            GameOver(computerSide);
        }
        else if (moveCount >= 9)
        {
            GameOver("draw");
        }
        else
        {
            ChangeSides();
            delay = 10;
        }
    }

    void ChangeSides()
    {
        //When this function is called, if it is the player's move, switch to computer and vice versa
        playerMove = (playerMove == true) ? false : true;
        if(playerMove == true)
        {
            SetPlayerColors(playerX, playerO);
        }
        else
        {
            SetPlayerColors(playerO, playerX);
        }
    }

    void SetPlayerColors(Player newPlayer, Player oldPlayer)
    {
        newPlayer.panel.color = activePlayerColor.panelColor;
        newPlayer.text.color = activePlayerColor.textColor;
        oldPlayer.panel.color = inactivePlayerColor.panelColor;
        oldPlayer.text.color = inactivePlayerColor.textColor;
    }

    void SetPlayerColorsInactive()
    {
        playerX.panel.color = inactivePlayerColor.panelColor;
        playerX.text.color = inactivePlayerColor.textColor;
        playerO.panel.color = inactivePlayerColor.panelColor;
        playerO.text.color = inactivePlayerColor.textColor;
    }

    void GameOver(string winningPlayer)
    {
        //Sets the game board as un-interactable and displays who won or if it was a draw
        SetBoardInteractable(false);
        if (winningPlayer == "draw")
        {
            SetGameOverText("It's a Draw!");
            SetPlayerColorsInactive();
        }
        else
        {
            SetGameOverText(winningPlayer + " Wins!");
        }
        restartButton.SetActive(true);
    }

    void SetGameOverText(string value)
    {
        //Enables game over panel
        gameOverPanel.SetActive(true);
        gameOverText.text = value;
    }

    public void RestartGame()
    {
        //Reset everything. Also randomize who goes first
        RandomizeStart();
        moveCount = 0;
        gameOverPanel.SetActive(false);
        SetBoardInteractable(true);
        delay = 10;

        for (int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].text = "";
        }
    }

    void SetBoardInteractable(bool toggle)
    {
        for (int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].GetComponentInParent<Button>().interactable = toggle;
        }
    }

    void RandomizeStart()
    {
        //Custom function. Completely randomizes whether the human or computer starts first.
        int rng = Random.Range(0, 2);
        if(rng == 0)
        {
            SetStartingSide("X");
            playerMove = true;
        }
        else
        {
            SetStartingSide("O");
            playerMove = false;
        }
    }

    bool WinDetected(string side)
    {
        //Custom function. Pass in a side as a string and check all win conditions
        if (buttonList[0].text == side
            && buttonList[1].text == side
            && buttonList[2].text == side)
        {
            return true;
        }
        else if (buttonList[3].text == side 
            && buttonList[4].text == side 
            && buttonList[5].text == side)
        {
            return true;
        }

        else if (buttonList[6].text == side
            && buttonList[7].text == side
            && buttonList[8].text == side)
        {
            return true;
        }

        else if (buttonList[0].text == side
            && buttonList[3].text == side
            && buttonList[6].text == side)
        {
            return true;
        }

        else if (buttonList[1].text == side
            && buttonList[4].text == side
            && buttonList[7].text == side)
        {
            return true;
        }

        else if (buttonList[2].text == side
            && buttonList[5].text == side
            && buttonList[8].text == side)
        {
            return true;
        }

        else if (buttonList[0].text == side
            && buttonList[4].text == side
            && buttonList[8].text == side)
        {
            return true;
        }

        else if (buttonList[2].text == side
            && buttonList[4].text == side
            && buttonList[6].text == side)
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    int CheckForPotentialWin(int panel1, int panel2)
    {
        //Checks to see if either the human player or computer is about to win. Assigns a value to the current tile accordingly
        int blockVal;
        Debug.ClearDeveloperConsole();
        
        if (buttonList[panel1].text == playerSide && buttonList[panel2].text == playerSide)
        {
            blockVal = 2;
        }
        else if(buttonList[panel1].text == computerSide && buttonList[panel2].text == computerSide)
        {
            blockVal = 5;       //Computer will prioritize winning over blocking!
        }
        else
        {
            blockVal = 1;
        }
        
        return blockVal;
    }

    void AI_Selection()
    {
        if(moveCount == 0)
        {
            RandomCorner();     //Choose a random corner if going first
        }
        else if(moveCount == 1)
        {
            //Prioritize the middle if the player chooses another tile for turn 0. Otherwise, pick a corner
            if (buttonList[4].GetComponentInParent<Button>().interactable == true)
            {
                value = 4;
            }
            else
            {
                RandomCorner();
            }
        }
        else
        {
            //Always prioritize the middle tile until it is selected
            if (buttonList[4].GetComponentInParent<Button>().interactable == true)
            {
                value = 4;
            }
            else
            {
                List<int> blockValues = new List<int>();        //List to store all the block values
                
                if(IsDiagonal_XOX() && moveCount == 3)
                {
                    ChooseAvailableEdge();
                }
                else if (IsDiagonal_XXO() && moveCount == 3)
                {
                    ChooseAvailableCorner();
                }
                else if(IsL_XOX() && moveCount == 3)
                {
                    int rng = Random.Range(0, 2);
                    if (buttonList[1].text == playerSide)
                    {
                        switch (rng)
                        {
                            case 0:
                                value = 0;
                                break;
                            case 1:
                                value = 2;
                                break;
                            default:
                                value = 0;  
                                break;
                        }
                    }
                    else if(buttonList[7].text == playerSide)
                    {
                        switch (rng)
                        {
                            case 0:
                                value = 6;
                                break;
                            case 1:
                                value = 8;
                                break;
                            default:
                                value = 6;  
                                break;
                        }
                    }
                }
                else
                {
                    blockValues.Add(AssignBlockVals(0, 1, 2, 3, 6, 4, 8));  //Top Left Corner
                    blockValues.Add(AssignBlockVals(1, 4, 7, 0, 2));        //Top Middle
                    blockValues.Add(AssignBlockVals(2, 0, 1, 5, 8, 4, 6));  //Top Right Corner
                    blockValues.Add(AssignBlockVals(3, 4, 5, 0, 6));        //Middle Left

                    blockValues.Add(0);                                     //Center Tile (this tile will ALWAYS be picked when in this else statement)

                    blockValues.Add(AssignBlockVals(5, 3, 4, 2, 8));        //Middle Right
                    blockValues.Add(AssignBlockVals(6, 7, 8, 0, 3, 2, 4));  //Bottom Left Corner
                    blockValues.Add(AssignBlockVals(7, 1, 4, 6, 8));        //Bottom Middle
                    blockValues.Add(AssignBlockVals(8, 6, 7, 2, 5, 0, 4));  //Bottom Right Corner

                    //Randomly select an index if multiple tiles have the same block value to the AI
                    int max = Mathf.Max(blockValues.ToArray());
                    List<int> indexList = new List<int>();
                    for (int i = 0; i < blockValues.Count; i++)
                    {
                        if (blockValues[i] == max)
                        {
                            indexList.Add(i);
                        }
                    }
                    int rng = Random.Range(0, indexList.Count);
                    value = indexList[rng];
                }
                blockValues.Clear();
            }
        } 
    }

    int AssignBlockVals(int current, int tile1, int tile2, int tile3, int tile4, int tile5, int tile6)
    {
        //Overload for corner tiles
        if (buttonList[current].GetComponentInParent<Button>().interactable == true)
            return FindCornerVal(tile1, tile2, tile3, tile4, tile5, tile6);   
        else
            return 0;
    }

    int AssignBlockVals(int current, int tile1, int tile2, int tile3, int tile4)
    {
        //Overload for edge tiles
        if (buttonList[current].GetComponentInParent<Button>().interactable == true)
            return FindEdgeVal(tile1, tile2, tile3, tile4);   
        else
            return 0;
    }

    int FindCornerVal(int tile1, int tile2, int tile3, int tile4, int tile5, int tile6)
    {
        //Return a block value for a corner tile
        List<int> indexList = new List<int>();
        indexList.Clear();
        indexList.Add(CheckForPotentialWin(tile1, tile2));
        indexList.Add(CheckForPotentialWin(tile3, tile4));
        indexList.Add(CheckForPotentialWin(tile5, tile6));
        return Mathf.Max(indexList.ToArray());
    }

    int FindEdgeVal(int tile1, int tile2, int tile3, int tile4)
    {
        //Return a block value for an edge tile
        List<int> indexList = new List<int>();
        indexList.Clear();
        indexList.Add(CheckForPotentialWin(tile1, tile2));
        indexList.Add(CheckForPotentialWin(tile3, tile4));
        return Mathf.Max(indexList.ToArray());
    }

   bool IsDiagonal_XOX()
    {
        if (buttonList[0].text == playerSide && buttonList[4].text == computerSide && buttonList[8].text == playerSide)
        {
            return true;
        }
        else if (buttonList[2].text == playerSide && buttonList[4].text == computerSide && buttonList[6].text == playerSide)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool IsL_XOX()
    {
        //check for two diagonal edges 
        if ((buttonList[1].text == playerSide && buttonList[3].text == playerSide) ||
            (buttonList[3].text == playerSide && buttonList[7].text == playerSide) ||
            (buttonList[7].text == playerSide && buttonList[5].text == playerSide) ||
            (buttonList[5].text == playerSide && buttonList[1].text == playerSide))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool IsDiagonal_XXO()
    {
        //Check if either diagonal is either XXO or OXX
        if ((buttonList[0].text == playerSide && buttonList[4].text == playerSide && buttonList[8].text == computerSide) ||
            (buttonList[0].text == computerSide && buttonList[4].text == playerSide && buttonList[8].text == playerSide))
        {
            return true;
        }
        else if ((buttonList[2].text == playerSide && buttonList[4].text == playerSide && buttonList[6].text == computerSide) ||
                 (buttonList[2].text == computerSide && buttonList[4].text == playerSide && buttonList[6].text == playerSide)) 
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void ChooseAvailableCorner()
    {
        List<int> availableCorners = new List<int>();   //List to store all corner tiles
        for (int i = 0; i < buttonList.Length; i += 2)   //Iterate through all corner tiles. Each even numbered tile is an edge (excluding middle tile)
        {
            if (buttonList[i].GetComponentInParent<Button>().interactable == true)
            {
                availableCorners.Add(i);                //Middle tile is always selected by this point, so it will never be added
            }
            //Pick a random edge
            int rng = Random.Range(0, availableCorners.Count);
            value = availableCorners[rng];
        }
        availableCorners.Clear();
    }

    void ChooseAvailableEdge()
    {
        List<int> availableEdges = new List<int>();     //List to store all edge tiles
        for (int i = 1; i < buttonList.Length; i += 2)   //Iterate through all edge tiles. Each odd numbered tile is an edge
        {
            if (buttonList[i].GetComponentInParent<Button>().interactable == true)
            {
                availableEdges.Add(i);  //Since this is only the 4th turn (moveCount == 3), there will always be at least one edge availble, so this is safe
            }
            //Pick a random edge
            int rng = Random.Range(0, availableEdges.Count);
            value = availableEdges[rng];
        }
        availableEdges.Clear();
    }

    void RandomCorner()
    {
        //Make it so the AI picks a random corner! (Best strategy to win)
        int cornerChoice = Random.Range(0, 4);
        switch (cornerChoice)
        {
            case 0:
                value = 0;
                break;
            case 1:
                value = cornerChoice + 1;
                break;
            case 2:
                value = cornerChoice + 4;
                break;
            case 3:
                value = cornerChoice + 5;
                break;
            default:
                value = 4;  //If something goes wrong, just have it start in the center
                break;
        }
    }
}