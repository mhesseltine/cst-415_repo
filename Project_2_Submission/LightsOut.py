import random

# Make a 2D array to represent the grid
grid = [[0,0,0],[0,0,0],[0,0,0]]
 
def predeterminedLayouts(number):
    """These layouts have been proven to allow for a solvable puzzle"""
    if (number == 1):
        grid[0][2] = 1
        grid[1][2] = 1
    elif(number == 2):
        grid[0][0] = 1
        grid[0][2] = 1
        grid[1][2] = 1
        grid[2][0] = 1
        grid[2][1] = 1
    elif(number == 3):
        grid[0][1] = 1
        grid[0][2] = 1
        grid[1][0] = 1
        grid[2][0] = 1
        grid[2][2] = 1        
    return

def printGrid():
    for i in range(len(grid[0])):
        print("\n")
        for j in range(len(grid[1])):
            print(grid[i][j], end="    ")
    print("\n")     
    
def nextMove():
    """This function determines what the next move shall be"""
    validPositions = []
    for i in range(len(grid[0])):
        for j in range(len(grid[1])):    # Double for-loop to iterate through each x,y position
            tally = 0                    # Tally will be used to record how many tiles surrounding the current tile are unlit
            if(grid[i][j] == 1):         # The constraint is that we cannot change the state of a lit tile, so continue if the tile is lit
                continue
            elif(grid[i][j] == 0):       # Only tally if the current tile is in the off state
                tally += 1               # Count the tile itself
                # Need to use Try-Except in case we go out of range
                if(( (i+1) != 3 ) and grid[i+1][j] == 0  ):        # Look one space down        
                    tally += 1
                if(grid[i-1][j] == 0 and ( (i-1) != -1) ):        # Look one space up
                    tally += 1
                if(( (j+1) != 3 ) and grid[i][j+1] == 0  ):  # Look one space right
                    tally += 1
                if(grid[i][j-1] == 0 and ( (j-1) != -1) ):        # Look one space left
                    tally += 1            
                validPositions.append([i, j, tally])    # Add to a list of positions that are valid to have their states changed. 
                                                        # Store x, y, and the tally
    
    tallyMax = 0                                    
    tallyPos = []    
    for items in range(len(validPositions)):        # Store the value for the greatest amount of unlit surrounding tiles
        if(validPositions[items][2] > tallyMax):
            tallyMax = validPositions[items][2]
    
    for items in range(len(validPositions)):        # Store the index in validPositions that the tallyMax is found
        if(validPositions[items][2] == tallyMax):
            tallyPos.append(validPositions[items])
    
    index = 0
    if(len(tallyPos) > 1):
        index = random.randint(0, len(tallyPos) - 1 )  # Randomly select the tile if more than one have the greatest tallyMax\
        return([tallyPos[index][0], tallyPos[index][1]])    
    else:
        return([tallyPos[0][0], tallyPos[0][1]])            # If there is no tie, just return the position with the tallyMax
                                
def initializeGrid(layout):
    """Initialize the grid with a random predetermined layout"""
    predeterminedLayouts(layout)

def makeMove(position):
    """When you make a move, change the state of the tile and all tiles surrounding it"""
    if(grid[position[0]] [position[1]] == 0):# Switch the current tile to on or off based on its current state
        grid[position[0]] [position[1]] = 1
        # Switch the neighbor tiles to on or off based on their current state

        # Evaluate tile to the up
    if(((position[0]+1)!=3) and grid[(position[0]+1)] [position[1]] == 1):
        grid[(position[0]+1)] [position[1]] = 0
    elif(((position[0]+1)!=3) and grid[(position[0]+1)] [position[1]] == 0):
        grid[(position[0]+1)] [position[1]] = 1
             
        # Evaluate tile to the down
    if(grid[(position[0]-1)] [position[1]] == 1 and (position[0]-1) != -1 ):
        grid[(position[0]-1)] [position[1]] = 0
    elif(grid[(position[0]-1)] [position[1]] == 0 and (position[0]-1) != -1):
        grid[(position[0]-1)] [position[1]] = 1 
        
        #Evaluate tile right
    if(((position[1]+1)!=3) and grid[position[0]] [(position[1] + 1)] == 1):
        grid[position[0]] [(position[1] + 1)] = 0
    elif(((position[1]+1)!=3) and grid[position[0]] [(position[1] + 1)] == 0):
        grid[position[0]] [(position[1] + 1)] = 1
                
        #Evaluate tile left
    if(grid[position[0]] [(position[1] - 1)] == 1 and (position[1] - 1) != -1):
        grid[position[0]] [(position[1] - 1)] = 0
    elif(grid[position[0]] [(position[1] - 1)] == 0 and (position[1] - 1) != -1):
        grid[position[0]] [(position[1] - 1)] = 1
   
    return

def isPuzzleSolved():
    for i in range(len(grid[0])):
        for j in range(len(grid[1])):
             if(grid[i][j] == 0):
                return 0
    return 1          

def main():
    randomize = random.randint(1,3)
    initializeGrid(randomize)
    printGrid()
    while (isPuzzleSolved() != 1): 
        makeMove(nextMove())
        printGrid()
    return 0
main()
